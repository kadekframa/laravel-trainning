<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Account Baru!</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="POST">
        @csrf;
        <h2>Sign Up Form</h2>
        <label for="firstname">First Name:</label>
        <br><br>
        <input type="text" name="firstname" placeholder="your first name...">
        <br><br>

        <label for="lastname">Last Name:</label>
        <br><br>
        <input type="text" name="lastname" placeholder="your last name...">
        <br><br>

        <label for="gender">Gender:</label>
        <br><br>
        <input type="radio" name="gender" value="male">Male</input>
        <br>
        <input type="radio" name="gender" value="femlae">Female</input>
        <br>
        <input type="radio" name="gender" value="other">Other</input>
        <br><br>

        <label for="nationality">Nationality:</label>
        <br><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select>
        <br><br>

        <label for="language">Language Spoken:</label>
        <br><br>
        <input type="checkbox" name="language" value="Indonesia">Bahasa Indonesia</input>
        <br>
        <input type="checkbox" name="language" value="English">English</input>
        <br>
        <input type="checkbox" name="language" value="Other">Other</input>
        <br><br>

        <label for="bio">Bio:</label>
        <br><br>
        <textarea name="bio" cols="30" rows="10" placeholder="add your bio here..."></textarea>
        <br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>