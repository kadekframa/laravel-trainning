@extends('layout.master')

@section('title')
    List Pemain Film
@endsection

@push('scripts')
  <script src="{{asset('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush

@push('styles')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>
@endpush

@section('content')
  <h1>{{$detail->nama}}</h1>
  <h5>Usia Cast: {{$detail->umur}}</h5>
  <p>{{$detail->bio}}</p>
  <a href="/cast" class="btn btn-secondary btn-sm">Back</a>
@endsection
