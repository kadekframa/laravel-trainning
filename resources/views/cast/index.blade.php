@extends('layout.master')

@section('title')
    List Pemain Film
@endsection

@push('scripts')
  <script src="{{asset('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush

@push('styles')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>
@endpush

@section('content')
    <a href="/cast/create" class="btn btn-primary btn-md mb-4">Add Cast</a>
    <table class="table table-bordered table-striped" id="example1">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama Cast</th>
            <th scope="col">Umur</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$cs)
            <tr>
              <th scope="row">{{$key + 1}}</th>
              <td>{{$cs->nama}}</td>
              <td>{{$cs->umur}}</td>
              <td class="d-flex justify-content-center">
                <form action="/cast/{{$cs->id}}" method="POST">
                  @csrf
                  @method('delete')
                  <a href="/cast/{{$cs->id}}" class="btn btn-info btn-sm mx-1">Detail</a>
                  <a href="/cast/{{$cs->id}}/edit" class="btn btn-warning btn-sm mx-1">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm mx-1">
                </form>
              </td>
            </tr>
            @empty
              <h1>Data Empty</h1>
            @endforelse
        </tbody>
      </table>
@endsection