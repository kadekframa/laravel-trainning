@extends('layout.master')

@section('title')
    Create Film Cast
@endsection

@section('content')
    <h1>Create Page</h1>
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
          <label for="exampleFormControlInput1">Nama</label>
          <input name="nama" type="text" class="form-control" id="exampleFormControlInput1" placeholder="Nama cast...">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <label for="exampleFormControlInput1">Umur</label>
          <input name="umur" type="number" class="form-control" id="exampleFormControlInput1" placeholder="Umur cast...">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <label for="exampleFormControlTextarea1">Biografi</label>
          <textarea name="bio" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Biografi cast..."></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
@endsection
