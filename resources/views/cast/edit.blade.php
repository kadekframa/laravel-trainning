@extends('layout.master')

@section('title')
    Edit Film Cast
@endsection

@section('content')
    <h1>Edit Page</h1>
    <form action="/cast/{{$edit->id}}" method="POST">
        @csrf
        @method('PUT');
        <div class="form-group">
          <label for="exampleFormControlInput1">Nama</label>
          <input name="nama" type="text" class="form-control" value="{{old('nama', $edit->nama)}}" id="exampleFormControlInput1" placeholder="Nama cast...">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <label for="exampleFormControlInput1">Umur</label>
          <input name="umur" type="number" class="form-control" value="{{old('umur', $edit->umur)}}" id="exampleFormControlInput1" placeholder="Umur cast...">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <label for="exampleFormControlTextarea1">Biografi</label>
          <textarea name="bio" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Biografi cast...">{{old('bio', $edit->bio)}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
@endsection
