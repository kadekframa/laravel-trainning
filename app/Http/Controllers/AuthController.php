<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register() {
        return view('halaman.register');
    }

    public function Welcome(Request $request) {
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language = $request['language'];
        $bio = $request['bio'];

        $data = ['firstname' => $firstname, 'lastname' => $lastname];

        return view('welcome', $data);
    }
}