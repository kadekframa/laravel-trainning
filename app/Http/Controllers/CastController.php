<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index () {
        $cast = DB::table('cast')->get();
 
        return view('cast.index', ['cast' => $cast]);
    }

    public function create() {
        return view('cast.add');
    }

    public function store(Request $request) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama harus diisi yaa..!',
            'umur.required' => 'Umur juga harus diisi yaa..!',
            'bio.required' => 'Bio wajib diisi yaa..!'
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);

        return redirect('/cast');
    }

    public function detail($id) {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.detail', ['detail'=>$cast]);
    }

    public function edit($id) {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit', ['edit'=>$cast]);
    }

    public function update(Request $request, $id) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama harus diisi yaa..!',
            'umur.required' => 'Umur juga harus diisi yaa..!',
            'bio.required' => 'Bio wajib diisi yaa..!'
        ]);

        DB::table('cast')->where('id', $id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);

        return redirect('/cast');
    }

    public function destroy($id) {
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
